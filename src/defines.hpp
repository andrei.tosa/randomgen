#pragma once

#define REG_SIZE 32
#define SET_SIZE 10000

#define F1(x, y, z) ((x) ^ ((y) | (z)))
#define F2(x, y, z) (!(x) ^ ((y) | (z)))

#define F(x, y, z) F1((x), (y), (z))


