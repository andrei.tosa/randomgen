#include <ap_int.h>
#include "defines.hpp"

extern "C" {
void rndgen(
		const unsigned int seed,
		const unsigned int count,
		unsigned int *out
	)
{
#pragma HLS INTERFACE m_axi port=out  offset=slave bundle=gmem
#pragma HLS INTERFACE s_axilite port=seed bundle=control
#pragma HLS INTERFACE s_axilite port=out  bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control


	ap_uint<REG_SIZE> X, Y;

	X = seed;
	
	generator: for (int i = 0; i < count; i++) {
#pragma HLS pipeline II=1
		for (int j = 1; j < REG_SIZE - 1; j++) {
#pragma HLS unroll factor=30
			Y[j] = F(X[j - 1], X[j], X[j+1]);
		}
		
		Y[0] = F(X[REG_SIZE - 1], X[0], X[1]);
		Y[REG_SIZE - 1] = F(X[REG_SIZE - 2], X[REG_SIZE - 1], X[0]);

		
		out[i] = Y.to_uint();
		X = Y;
	}
}
}
