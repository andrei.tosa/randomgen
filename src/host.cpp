#include "host.hpp"
//#include "defines.hpp"
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <XCLBIN File>" << std::endl;
		return EXIT_FAILURE;
	}

    std::string binaryFile = argv[1];
    size_t vector_size_bytes = sizeof(int) * SET_SIZE;
    cl_int err;
    unsigned fileBufSize;
    // Allocate Memory in Host Memory
   	uint32_t seed = 12345;
   	uint32_t count = SET_SIZE;
    std::vector<uint32_t,aligned_allocator<uint32_t>> source_hw_results(SET_SIZE);
    std::vector<uint32_t,aligned_allocator<uint32_t>> source_sw_results(SET_SIZE);


    // Create the test data 
    for(int i = 0 ; i < SET_SIZE ; i++)
		source_hw_results[i] = 0;

	populate_results(source_sw_results, seed);

// OPENCL HOST CODE AREA START
	
    std::vector<cl::Device> devices = get_devices("Xilinx");
    devices.resize(1);
    cl::Device device = devices[0];

    OCL_CHECK(err, cl::Context context(device, NULL, NULL, NULL, &err));
    OCL_CHECK(err, cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err));

    char* fileBuf = read_binary_file(binaryFile, fileBufSize);
    cl::Program::Binaries bins{{fileBuf, fileBufSize}};
	
	OCL_CHECK(err, cl::Program program(context, devices, bins, NULL, &err));
	OCL_CHECK(err, cl::Kernel krnl_rnd_gen(program,"rndgen", &err));

    OCL_CHECK(err, cl::Buffer buffer_output(context,CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, vector_size_bytes, source_hw_results.data(), &err));

    OCL_CHECK(err, err = krnl_rnd_gen.setArg(0, seed));
    OCL_CHECK(err, err = krnl_rnd_gen.setArg(1, count));
    OCL_CHECK(err, err = krnl_rnd_gen.setArg(2, buffer_output));

    OCL_CHECK(err, err = q.enqueueTask(krnl_rnd_gen));
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_output},CL_MIGRATE_MEM_OBJECT_HOST));

    q.finish();
	std::cout << "* * * SIMULATION DONE\n";	

// OPENCL HOST CODE AREA END


    // Compare the results of the Device to the simulation
    std::ofstream fout("data.out");
    bool match = true;
    for (int i = 0 ; i < SET_SIZE; i++){
        fout << source_hw_results[i] << '\n';
		
		if (source_sw_results[i] != source_hw_results[i]) {
			match = false;	
			std::cout << "Not matched: " << source_sw_results[i] << ' ' << source_hw_results[i] << '\n'; 
			break;
		}
    }


	fout.close();
    delete[] fileBuf;

    std::cout << "TEST " << (match ? "PASSED" : "FAILED") << std::endl; 
    return (match ? EXIT_SUCCESS : EXIT_FAILURE);
}

